FROM node:lts

WORKDIR /myapp

COPY package.json .
COPY server.js .

RUN npm install  

#Feed the pm2 config the container
CMD ["node", "/myapp/server.js"]
